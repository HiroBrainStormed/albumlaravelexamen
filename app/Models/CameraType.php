<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CameraType extends Model
{
    protected $fillable = [
        'name'
    ];
}
