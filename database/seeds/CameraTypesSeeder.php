<?php

use Illuminate\Database\Seeder;
use App\Models\CameraType;

class CameraTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CameraType::create([
            'name' => 'Smartphone'
        ]);

        CameraType::create([
            'name' => 'DSLR'
        ]);

        CameraType::create([
            'name' => 'Prosommateur'
        ]);

        CameraType::create([
            'name' => 'Professionelle'
        ]);

        CameraType::create([
            'name' => 'SuperConcentrée'
        ]);

        CameraType::create([
            'name' => 'Dédiée'
        ]);
    }
}
